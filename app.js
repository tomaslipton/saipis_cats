/**
 * Точка входа
 */

//подключаем модули
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
// var exphbs = require('express-handlebars');
var expressValidator = require('express-validator');
var flash = require('connect-flash');
var session = require('express-session');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongo = require('mongodb');
var mongoose = require('mongoose');
var mustacheExpress = require('mustache-express');
var Mustache = require('mustache');

//устанавливаем соединение с БД
mongoose.connect('mongodb://localhost/cats');
var db = mongoose.connection;

//роуты для обработки ссылок
var routes = require('./routes/index');
var users = require('./routes/users');

// Запускаем сервер
var app = express();

//Устанавливаем шаюлонизатор
app.set('view engine', 'mustache');
app.engine('mustache', mustacheExpress());
app.set('views', __dirname + '/views');
app.use(express.static(__dirname + '/public'));

// настройки
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

// Указываем папку для статичных ресурсов
app.use(express.static(path.join(__dirname, 'public')));

// Настраиваем сессию
app.use(session({
    secret: 'secret',
    saveUninitialized: true,
    resave: true,
}));

// PПодключаем библиотеку для авторизации
app.use(passport.initialize());
app.use(passport.session());

// Express Validator
app.use(expressValidator({
    errorFormatter: function (param, msg, value) {
        var namespace = param.split('.')
            , root = namespace.shift()
            , formParam = root;

        while (namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }
        return {
            param: formParam,
            msg: msg,
            value: value
        };
    }
}));

// Подключаем флеш для передачи сообщений между старницами
app.use(flash());

// Глобальные сообщения об ошибках
app.use(function (req, res, next) {
    res.locals.success_msg = req.flash('success_msg'); //Для успешных сообщений
    res.locals.error_msg = req.flash('error_msg'); // сообщений об ошибке
    res.locals.error = req.flash('error'); // содержание ошибки
    res.locals.user = req.user || null; // пользователя, если не авторизирован, то null
    next();
});

//Подключаем роуты
app.use('/', routes);
app.use('/users', users);

// Порт для запуска
app.set('port', 8000);

// Запускаем сервер
app.listen(app.get('port'), function () {
    console.log('Server started on port ' + app.get('port'));
});
let router = require('express').Router(); // модль для обработки адресов

router.get('/', ensureAuthenticated, function (req, res) {
    res.render('index', {title: "Welcome"});// выводим шаблон индекс и в него передаем заголовок
});

router.get('/toshka', ensureAuthenticated, function (req, res) {
    res.render('cats/toshka');
});

router.get('/corica', ensureAuthenticated, function (req, res) {
    res.render('cats/corica');
});

router.get('/rizhik', ensureAuthenticated, function (req, res) {
    res.render('cats/rizhik');
});

router.get('/tishka', ensureAuthenticated, function (req, res) {
    res.render('cats/tishka');
});

router.get('/tosya-nastysha', ensureAuthenticated, function (req, res) {
    res.render('cats/tosya+nastysha');
});


//Проверяем авторизирован ли пользователь
function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    } else {
        req.flash('error_msg','Сначала авторизируйтесь!');
        res.redirect('/users/login');
    }
}

//нахвание файла для подключения его в другие файлы
module.exports = router;
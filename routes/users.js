// Модули
var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');

// Страничка регистрации
router.get('/register', function (req, res) {
    res.render('register', {title: 'Регистация'});
});

// //страничка логина
router.get('/login', function (req, res) {
    var error = req.flash('msg'); // сообщение об ошибки
    res.render('login', {
        title: 'Авторизация',
        message: error
    });
});

// Обработка данных пришедших со сстраницы регистрации
router.post('/register', function (req, res) {
    //поля
    var name = req.body.name;
    var email = req.body.email;
    var username = req.body.username;
    var password = req.body.password;
    var password2 = req.body.password2;

    // Валидация
    req.checkBody('name', 'Пожайлуйста, введите имя').notEmpty();
    req.checkBody('email', 'Пожалуйста, введите email').notEmpty();
    req.checkBody('email', 'Email некорректный').isEmail();
    req.checkBody('username', 'Пожалуйста, введите логин').notEmpty();
    req.checkBody('password', 'Пожалуйста, введите пароль').notEmpty();
    req.checkBody('password2', 'Пароли не совпадают').equals(req.body.password);

    var errors = req.validationErrors();

    if (errors) {
        //вывод ошибок
        res.render('register', {
            errors: errors,
            profile: req.body
        });
    } else {
        //создать новго пользователя
        var newUser = new User({
            name: name,
            email: email,
            username: username,
            password: password
        });

        //сохранить в базу данных
        User.createUser(newUser, function (err, user) {
            if (err) throw err;
        });

        req.flash('msg', 'Регистрация успешна. Введите, пожалуйста, свой логин и пароль');

        res.redirect('/users/login'); // перенаправление
    }
});


// настройка паспорта для авторизации

passport.use('local-login', new LocalStrategy({
    usernameField : 'username',
    passwordField : 'password'
}, function (username, password, done) {
    User.getUserByUsername(username, function (err, user) {
        if (err) throw err;
        if (!user) {
            console.log('Неизвестный');
            return done(null, false, {message: 'Неизвестный пользователь'});
        }

        User.comparePassword(password, user.password, function (err, isMatch) {
            if (err) throw err;
            if (isMatch) {
                return done(null, user);
            } else {
                console.log(['Неверный']);
                return done(null, false, {message: 'Неверный пароль'});
            }
        });
    });
}));

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    User.getUserById(id, function (err, user) {
        done(err, user);
    });
});

//обработка данных сос транциы автиоризации
router.post('/login',
    passport.authenticate('local-login', {
        successRedirect: '/',
        failureRedirect: '/users/login',
        failureFlash: true
    }));

// для выхода из аккаунта
router.get('/logout', function (req, res) {
    req.logout();

    req.flash('success_msg', 'Вы вышли из аккаунта. Скорее возвращайтесь.');

    res.redirect('/users/login');
});

module.exports = router;